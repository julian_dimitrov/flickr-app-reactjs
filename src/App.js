import React, { Component } from 'react';
import './App.css';
import Gallery from './components/Gallery.js';
import Header from './components/Header.js';
import Footer from './components/Footer.js';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterText: ''
        }
    }

  render() {
    return (
      <div className="App">
          <Header/>
          <div id="content">
              <Gallery/>
          </div>
          <Footer/>
      </div>
    );
  }
}

export default App;
