import React, {Component} from 'react'
import LazyLoad from 'react-lazyload';
import { TransitionGroup, CSSTransition } from 'react-transition-group'

export default class Images extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedImage: ''
        };
    }

    // assemble image URL
    imageURL(item) {
        return 'http://farm' + item.farm + '.staticflickr.com/' + item.server + '/' + item.id + '_' + item.secret + '.jpg';
    }

    userURL(item) {
        return 'http://flickr.com/' + item.owner;
    }

    // handle image selection
    selectImage(selectedImage) {
        this.setState({
            selectedImage
        })
    }

    _renderItems() {
        var _this = this;

        return this.props.items.map(function(item, index) {
            return (
                <div className="media-gallery-holder col-lg-3 col-md-4 col-sm-6 col-xs-12" key={index}>
                    <div className="media-gallery-item" key={index} onClick={_this.selectImage.bind(_this,_this.imageURL(item))}>
                        <div className="image">
                            <LazyLoad height={120} throttle={400}>
                                <CSSTransition in={true} key={index} classNames="example" timeout={{ enter: 500, exit: 300 }}>
                                    <div className="img" style={{
                                        backgroundImage: "url(" + _this.imageURL(item) + ")"
                                    }}> </div>
                                </CSSTransition>
                            </LazyLoad>
                        </div>
                        <div className="more-info">
                            <div className="row top-info">
                                <div className="col-md-7 text-left">
                                    <a target="_blank" href={_this.imageURL(item)}><span className="title">{item.title}</span></a>
                                </div>
                                <div className="col-md-5 text-right">
                                    <span className="author">by <a target="_blank" href={_this.userURL(item)}>{item.ownername}</a></span>
                                </div>
                            </div>
                            <p className="description" dangerouslySetInnerHTML={{ __html: item.description._content.length > 150 ? item.description._content.substr(0, 150) + '...' : item.description._content  }}></p>
                        </div>
                        <div className="tags">Tags: {item.tags ? (item.tags.length > 35 ? item.tags.substr(0,35).replace(/ /g, ', ') + '...' : item.tags.replace(/ /g, ', ')) : 'No tags'}</div>
                    </div>
                </div>

            );
        });
    }

    // render the app
    render() {
        return (
            <TransitionGroup>
                {this._renderItems()}
            </TransitionGroup>
        )
    }
}
