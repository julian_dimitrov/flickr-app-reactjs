import React, {Component} from 'react'

export default class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filterText: ''
        };
        this.searchUpdated = this.searchUpdated.bind(this);
    }

    searchUpdated(e) {
        var _this = this;
        if (e.target.value.length > 2) {
            _this.setState({filterText: e.target.value});
            const newState = e.target.value;
            _this.props.callbackParent(newState);
        } else {
            _this.props.callbackParent('');
        }

    }

    // render the app
    render() {
        return (
            <form id="search">
                <input
                    className="form-control"
                    type="text"
                    placeholder="Search by tag..."
                    onChange={this.searchUpdated}
                />
            </form>
        )
    }
}
