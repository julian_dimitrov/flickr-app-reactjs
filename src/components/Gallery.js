import React, {Component} from 'react'
import axios from 'axios'
import Search from "./Search";
import Images from "./Images";

import Waypoint from 'react-waypoint';


export default class Gallery extends Component {

    // set initial state of elements
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filterText: '',
            page: 1,
            isLoading: false
        };
        this.onFilterUpdated = this.onFilterUpdated.bind(this);
    }

    // make API request
    // TODO: move it to its own 'action'
    // TODO: remove hardcoded variables such as API KEY and params and turn them into variables
    componentDidMount() {
      this.onFilterUpdated();
    }

    onFilterUpdated(filter) {
        var _this = this;

        var currentItems = this.state.items;

        if (_this.state.filterText === filter) {
            _this.setState({ page: _this.state.page+1, isLoading: true});
        } else {
            _this.setState({ filterText: filter, page: 1, isLoading: true, items: [] });
            currentItems = [];
        }


        var url = 'https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=260e9d986d03661e281fdae789d72cdc&extras=description,tags,owner_name&format=json&&nojsoncallback=1&per_page=12&page=' + _this.state.page;

        if (filter) {
            url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=260e9d986d03661e281fdae789d72cdc&extras=description,tags,owner_name&tags=' + filter + '&format=json&&nojsoncallback=1&per_page=12&page=' + _this.state.page;
        }

        this.serverRequest =
        axios
            .get(url)
            .then(function(result) {
                window.setTimeout(function() {
                    for (var i = 0; i < result.data.photos.photo.length; i++) {
                        currentItems.push(result.data.photos.photo[i]);
                    }

                    _this.setState({
                        items: currentItems,
                        isLoading: false
                    })
                }.bind(this), 1000);
            });
    }

    _renderWaypoint() {
        var _this = this;

        if (!_this.state.isLoading) {
            return (
                <Waypoint
                    topOffset={'75%'}
                    bottomOffset={'-5%'}
                    scrollableAncestor={window}
                    onEnter={() => this.onFilterUpdated(_this.state.filterText)}
                />
            );
        }
    }

    // render the app
    render() {
        return (
            <div className="container">
                <Search callbackParent={(newState) => this.onFilterUpdated(newState) } />
                <div className="media-gallery">
                    <div className="row">
                        <div className="infinite-scroll-example__scrollable-parent">
                            <Images items={this.state.items} />
                            <div className="clearfix"></div>
                            <div className="infinite-scroll-example__waypoint">
                                {this._renderWaypoint()}
                                <div class="sk-circle">
                                    <div class="sk-circle1 sk-child"></div>
                                    <div class="sk-circle2 sk-child"></div>
                                    <div class="sk-circle3 sk-child"></div>
                                    <div class="sk-circle4 sk-child"></div>
                                    <div class="sk-circle5 sk-child"></div>
                                    <div class="sk-circle6 sk-child"></div>
                                    <div class="sk-circle7 sk-child"></div>
                                    <div class="sk-circle8 sk-child"></div>
                                    <div class="sk-circle9 sk-child"></div>
                                    <div class="sk-circle10 sk-child"></div>
                                    <div class="sk-circle11 sk-child"></div>
                                    <div class="sk-circle12 sk-child"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}