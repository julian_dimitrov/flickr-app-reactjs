import React, {Component} from 'react'

export default class Header extends Component {

    render() {
        return (
            <header className="App-header">
                <div className="container">
                    <h1 className="App-title">Flickr App</h1>
                </div>
            </header>
        )
    }
}
