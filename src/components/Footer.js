import React, {Component} from 'react'

export default class Footer extends Component {

    render() {
        return (
            <footer className="footer">
                <div className="container">
                    <span className="copyright">© 2018 Julian Dimitrov</span>
                </div>
            </footer>
        )
    }
}
